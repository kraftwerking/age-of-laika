//
//  KRAFTWERKINGViewController.m
//  Age of Laika
//
//  Created by RJ Militante on 12/18/14.
//  Copyright (c) 2014 Kraftwerking LLC. All rights reserved.
//

#import "KRAFTWERKINGViewController.h"

@interface KRAFTWERKINGViewController ()

@end

@implementation KRAFTWERKINGViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)convertToDogYears:(UIButton *)sender {
    int humanYears = [self.dogYearsNumberTextField.text intValue];
    NSLog(@"Number of human years %i", humanYears);
    int numberOfDogYears = humanYears * 7;
    self.yearsLabel.text = [NSString stringWithFormat:@"%i",numberOfDogYears];
}


- (IBAction)convertToRealDogYears:(UIButton *)sender {
    int humanYears = [self.dogYearsNumberTextField.text intValue];
    int dogYears;
    if(humanYears > 2){
        dogYears = (10.5 * 2) + ((humanYears - 2) * 4);
    }
    else {
        dogYears = 10.5 * humanYears;
    }
    self.realYears.text = [NSString stringWithFormat:@"%i",dogYears];
}


@end
