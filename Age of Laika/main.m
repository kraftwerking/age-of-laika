//
//  main.m
//  Age of Laika
//
//  Created by RJ Militante on 12/18/14.
//  Copyright (c) 2014 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KRAFTWERKINGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KRAFTWERKINGAppDelegate class]));
    }
}
