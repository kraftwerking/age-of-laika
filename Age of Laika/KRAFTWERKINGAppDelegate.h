//
//  KRAFTWERKINGAppDelegate.h
//  Age of Laika
//
//  Created by RJ Militante on 12/18/14.
//  Copyright (c) 2014 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KRAFTWERKINGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
