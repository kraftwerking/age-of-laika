//
//  KRAFTWERKINGViewController.h
//  Age of Laika
//
//  Created by RJ Militante on 12/18/14.
//  Copyright (c) 2014 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KRAFTWERKINGViewController : UIViewController


@property (strong, nonatomic) IBOutlet UILabel *dogYearsLabel;

@property (strong, nonatomic) IBOutlet UITextField *dogYearsNumberTextField;

@property (strong, nonatomic) IBOutlet UILabel *yearsLabel;


@property (strong, nonatomic) IBOutlet UIButton *convertToDogYears;

@property (strong, nonatomic) IBOutlet UILabel *realYearsLabel;

@property (strong, nonatomic) IBOutlet UILabel *realYears;

- (IBAction)convertToRealDogYears:(UIButton *)sender;

@end
